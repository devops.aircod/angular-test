import { Component, ViewChild, OnInit, Input } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import TreeView from "devextreme/ui/tree_view";
import TextBox from "devextreme/ui/text_box";
// import {DxTreeViewComponent} from  "devextreme-angular"
// import DropDownBox from 'devextreme/ui/drop_down_box';
import ContextMenu from 'devextreme/ui/context_menu'
import Popover from 'devextreme/ui/popover'
import { cachedDataVersionTag } from 'v8';

@Component({
  selector: 'app-analysis-panel',
  templateUrl: './analysis-panel.component.html',
  styleUrls: ['./analysis-panel.component.scss']
})
export class AnalysisPanelComponent implements OnInit {
  @Input() Devices: any = []; // Analysis Devices
  @Input() GlobalAnalysis: any = []; // Global Analysis
  @Input() leftMenuOpen: boolean = false; // Analysis Menu Bool
  @ViewChild('tabset') tabset: TabsetComponent;
  contextMenuItems: any; // Analysis Context Menu Items
  currentAnalysis: any; // Current Analysis


  popupVisible = false; // For Show/Hide Analysis Popup
  deleteConfirmationPopUp = false; //For Show/Hide Delete Confirmation PopUp
  editConfirmationPopUp = false;

  updateGlobalPopup=false;
  deleteGlobalPopup =false;



  text:any;
  currentId: any; // It updates with the ID that is right clicked
  childLength: any;  // It updates with the length of Items currently in the child
  upcomingChildInd: any // It adds 1 to the current ID to make the next id unique
  globalPopup = false;// Used for Global Analysis Show/Hide
  main_global = false; // Used to check if Global Item Popup opened by "+" icon or right click
  updateGlobal: any; // It updates with the current right clicked item when clicked on any Global Analysis Item
  selectedItem: any; // Selected Item for Deletion
  // @Input() currentId:string = '';  // Used to Generate ID for Custom Analysis 
  // @Input() currentIndex:any = '';  // Used to Check On Which Node Custom Analysis to be Added
  // @ViewChild(DxTreeViewComponent, { static: false }) treeView;

  // analysis_name: string;
  // select_analaysis: string;
  // treeBoxValue:string;



  constructor() {
    this.GlobalAnalysis = [{
      id: "1",
      text: "GSM",
      addChild: true,
      items: []
    }, {
      id: "2",
      text: "UMTS",
      addChild: true,
      items: []
    }, {
      id: "3",
      text: "LTE",
      addChild: true,
      items: []
    }, {
      id: "4",
      text: "5G",
      addChild: true,
      items: []
    }];
    this.updateGlobal = [];
    // Devices 
    this.Devices = [{
      id: "1",
      text: "Device id: 0001",
      items: [{
        id: "1_1",
        text: "Device Info",
        items: [{
          id: "1_1_1",
          text: "Info"
        }, {
          id: "1_1_2",
          text: "Device",
        }, {
          id: "1_1_3",
          text: "Script",
        }]
      }, {
        id: "1_2",
        text: "Log Files",
        items: [{
          id: "1_2_1",
          text: "File 01"
        }, {
          id: "1_2_2",
          text: "File 02",
        }, {
          id: "1_2_3",
          text: "File 03",
        }]
      }, {
        id: "1_3",
        text: "Analysis",
        items: [{
          id: "1_3_1",
          text: "GPS"
        }, {
          id: "1_3_2",
          text: "GSM",
        }, {
          id: "1_3_3",
          text: "UMTS",
        }, {
          id: "1_3_4",
          text: "LTE",
        }, {
          id: "1_3_5",
          text: "5G",
        }]
      }, {
        id: "1_4",
        text: "Custom Analysis",
        // analysis:[
        //   {"ID": "101","name": "GPS",},
        //   {ID": "102","name": "GSM",},
        //   {"ID": "103","name": "UMTS"},
        //   {"ID": "104","name": "LTE"}
        // ],
        addChild: true,
        items: [{
          id: "1_4_1",
          text: "GSM",
          addChild: true,
          items: []
        }, {
          id: "1_4_2",
          text: "UMTS",
          addChild: true,
          items: []
        }, {
          id: "1_4_3",
          text: "LTE",
          addChild: true,
          items: []
        }, {
          id: "1_4_4",
          text: "5G",
          addChild: true,
          items: []
        }]
      }]
    },
    {
      id: "2",
      text: "Device id: 0002",
      items: [{
        id: "2_1",
        text: "Device Info",
        items: [{
          id: "2_1_1",
          text: "Info"
        }, {
          id: "2_1_2",
          text: "Device",
        }, {
          id: "2_1_3",
          text: "Script",
        }]
      }, {
        id: "2_2",
        text: "Log Files",
        items: [{
          id: "2_2_1",
          text: "File 01"
        }, {
          id: "2_2_2",
          text: "File 02",
        }, {
          id: "2_2_3",
          text: "File 03",
        }]
      }, {
        id: "2_3",
        text: "Analysis",
        items: [{
          id: "2_3_1",
          text: "GPS"
        }, {
          id: "2_3_2",
          text: "GSM",
        }, {
          id: "2_3_3",
          text: "UMTS",
        }, {
          id: "2_3_4",
          text: "LTE",
        }, {
          id: "2_3_5",
          text: "5G",
        }]
      }, {
        id: "2_4",
        text: "Custom Analysis",
        // analysis:[
        //   {"ID": "201","name": "GPS",},
        //   {ID": "202","name": "GSM",},
        //   {"ID": "203","name": "UMTS"},
        //   {"ID": "204","name": "LTE"}
        // ],
        addChild: true,
        items: [{
          id: "2_4_1",
          text: "GSM",
          addChild: true,
          items: []
        }, {
          id: "2_4_2",
          text: "UMTS",
          addChild: true,
          items: []
        }, {
          id: "2_4_3",
          text: "LTE",
          addChild: true,
          items: []
        }, {
          id: "2_4_4",
          text: "5G",
          addChild: true,
          items: []
        }]
      }]
    }];
  }

  ngOnInit(): void {
  }


  updateContextItems(event) {
    let analysisContentMenu = document.getElementById("analysisContentMenu");
    let analysisContentMenu_instance = ContextMenu.getInstance(analysisContentMenu) as ContextMenu;
    if (event.itemData.addChild == true) {
      analysisContentMenu_instance.option('items', [
        { text: 'Map View' },
        { text: 'Chart View' },
        { text: 'Grid View' },
        { text: 'Create Query' },
        { text: 'Add Child' },
        { text: 'Delete' },
        { text: 'Rename' },

      ]);
    }
    else {
      analysisContentMenu_instance.option('items', [
        { text: 'Map View' },
        { text: 'Chart View' },
        { text: 'Grid View' },
      ]);
    }
    this.currentAnalysis = event;
  }

  itemClick(event) {
    if (event.itemIndex == 4) {//Checking Index of Add Child Node in Context Menu
      this.childLength = this.currentAnalysis.node.items.length;//Current Children
      this.upcomingChildInd = this.childLength + 1; //Add one To generate new Id for the Child Node
      this.currentId = this.currentAnalysis.itemData.id; //Get Current Node ID
      this.popupVisible = true;
     this.text="";
      // childPopover_instance.option('target', "#" + this.currentAnalysis.itemElement.offsetParent);
    }
    else if (event.itemIndex == 5) {
      let currentChildren = this.currentAnalysis.node.itemData.id;
      this.deleteConfirmationPopUp = true;
    } else if (event.itemIndex == 6) {
      this.text=this.currentAnalysis.itemData.text;
      this.editConfirmationPopUp = true;
     
    }
  }

  addAnalaysis(event) {
   
    let result = event.validationGroup.validate();
    let simpleText=this.text;
    if (result.isValid) {

      let analysis_name = document.getElementById("analysis_name");
      let analysis_name_instance = TextBox.getInstance(analysis_name) as TextBox;
      let name = analysis_name_instance.option('value');
      // analysis_name_instance.option('value', '');


      let newChild = {
        id: this.currentId + "_" + this.upcomingChildInd,
        text: simpleText,
        addChild: true,
        items: []
      }
       analysis_name_instance.option('value', '');

      this.currentAnalysis.itemData.items.push(newChild)// Push Child in The Current Cliked Node

      // Update Main Object
      var index = this.Devices.findIndex(item => item.id === this.currentId)
      this.Devices.splice(index, newChild)

      // Reload Tree wiith the Updated Main Object
      let selection_treeview = document.getElementById("selection-treeview");
      let selection_treeview_instance = TreeView.getInstance(selection_treeview) as TreeView;
      selection_treeview_instance.option('items', this.Devices);

      this.popupVisible = false;
    }

  }

    //CODE TO DELETE THE SELECTED ITEM
    deleteAnalaysis($event) {
      this.deleteSelectedAnalysis(this.Devices);
      let selection_treeview = document.getElementById("selection-treeview");
      let selection_treeview_instance = TreeView.getInstance(selection_treeview) as TreeView;
      selection_treeview_instance.option('items', this.Devices);
      this.deleteConfirmationPopUp = false;
    }
  
    deleteSelectedAnalysis(devices) {
      if (devices.length === 0) {
        return;
      }
      else {
        for (let i = 0; i < devices.length; i++) {
          if (devices[i].id === this.currentAnalysis.itemData.id) {
            devices.splice(i, 1);
            return;
          } else {
            if (devices[i].items) {
              this.deleteSelectedAnalysis(devices[i].items);
            }
          }
        }
      }
    }
  
  
  
    updateAnalaysis($event) {
      this.currentAnalysis.itemData.text=this.text;
      this.editConfirmationPopUp = false;
      this.text="";
    }
  
    cancel($event) {
      if (this.editConfirmationPopUp) {
        this.editConfirmationPopUp = !this.editConfirmationPopUp;
      } else if (this.deleteConfirmationPopUp) {
        this.deleteConfirmationPopUp = !this.deleteConfirmationPopUp;
      }else if(this.deleteGlobalPopup){
        this.deleteGlobalPopup=false;
      }
  
    }


  globalAnalysisPopup(event) {
    this.globalPopup = !this.globalPopup;
    this.main_global = true;
  }


  updateContextGlobal(event) {
    let globalContentMenu = document.getElementById("globalContentMenu");
    let globalContentMenu_instance = ContextMenu.getInstance(globalContentMenu) as ContextMenu;

    globalContentMenu_instance.option('items', [
      { text: 'Map View' },
      { text: 'Chart View' },
      { text: 'Grid View' },
      { text: 'Create Query' },
      { text: 'Add Child' },
      { text: 'Delete' },
      { text: 'Rename' },
    ]);

    this.updateGlobal = event;
  }







  globalitemClick(event) {
    if (event.itemIndex == 4) {//Checking Index of Add Child Node in Context Menu
      this.childLength = this.updateGlobal.node.items.length;//Current Children
      this.upcomingChildInd = this.childLength + 1; //Add one To generate new Id for the Child Node
      this.currentId = this.updateGlobal.itemData.id; //Get Current Node ID
      this.globalPopup = true;
    }else if(event.itemIndex == 5){
     
this.deleteGlobalPopup=true;
    }else if(event.itemIndex == 6){

      this.text=this.updateGlobal.itemData.text;
      this.updateGlobalPopup=true;
    }
  }

  globalAnalysisClick(event) {
    let result = event.validationGroup.validate();
    if (result.isValid) {
      let global_analysis_name = document.getElementById("global_analysis_name");
      let global_analysis_name_instance = TextBox.getInstance(global_analysis_name) as TextBox;
      let global_name = global_analysis_name_instance.option('value');


      if (this.main_global == true) {
        let global_length = this.GlobalAnalysis.length;
        let global_id = global_length + 1;

        let globalChild = {
          id: global_id,
          text: this.text,
          addChild: true,
          items: []
        }

        this.GlobalAnalysis.push(globalChild)
      }
      else {
        console.log(this.currentId);
        console.log(this.upcomingChildInd);

        let globalChild = {
          id: this.currentId + "_" + this.upcomingChildInd,
          text: global_name,
          addChild: true,
          items: []
        }

        this.updateGlobal.itemData.items.push(globalChild)// Push Child in The Current Cliked Node

        // Update Main Object
        var index = this.GlobalAnalysis.findIndex(item => item.id === this.currentId)
        this.GlobalAnalysis.splice(index, globalChild)

      }

      // Reload Tree wiith the Updated Main Object
      let global_treeview = document.getElementById("global-treeview");
      let global_treeview_instance = TreeView.getInstance(global_treeview) as TreeView;
      global_treeview_instance.option('items', this.GlobalAnalysis);

      this.globalPopup = false;
      global_analysis_name_instance.option('value', '');
    }

    this.main_global = false;
  }

  updateGlobalAnalysis($event){
    this.updateGlobal.itemData.text=this.text;
     this.updateGlobalPopup=false;
     this.text="";
  }

  deleteGlobalAnalaysis($event) {
    this.deleteSelectedGlobalAnalysis(this.GlobalAnalysis);
    let global_treeview = document.getElementById("global-treeview");
    let global_treeview_instance = TreeView.getInstance(global_treeview) as TreeView;
    global_treeview_instance.option('items', this.GlobalAnalysis);

    this.deleteGlobalPopup = false;
  }

  deleteSelectedGlobalAnalysis(devices) {
    if (devices.length === 0) {
      return;
    }
    else {
      for (let i = 0; i < devices.length; i++) {
        if (devices[i].id === this.updateGlobal.itemData.id) {
          devices.splice(i, 1);
          return;
        } else {
          if (devices[i].items) {
            this.deleteSelectedGlobalAnalysis(devices[i].items);
          }
        }
      }
    }
  }





  // When Click on Plus Icon In Front of Custom Anlysis
  // getIndex(e,i){
  //   this.currentId = i; 
  //   this.currentIndex = i.split("_")[0];
  //   this.currentIndex = this.currentIndex - 1;

  //   let customAnalysis =  [this.Devices[this.currentIndex]];
  //   console.log(customAnalysis);
  //   setTimeout(function(){
  //     debugger;
  //     let select_analaysis = document.getElementById("select_analaysis");
  //     let select_analysis_instance = DropDownBox.getInstance(select_analaysis) as DropDownBox;
  //     select_analysis_instance.option('items', customAnalysis);
  //   },1000)
  // }

  // addAnalaysis(e){
  //   let result = e.validationGroup.validate();
  //   if (result.isValid) {
  //     // debugger;
  //     //Get Selected Value in Select Box 
  //     let select_analaysis = document.getElementById("select_analaysis");
  //     let select_analysis_instance = DropDownBox.getInstance(select_analaysis) as DropDownBox;
  //     let value = select_analysis_instance.option('text');
  //     select_analysis_instance.option('text','');

  //     //Get Typed Value in Text Box 
  //     let analysis_name = document.getElementById("analysis_name");
  //     let analysis_name_instance = TextBox.getInstance(analysis_name) as TextBox;
  //     let name = analysis_name_instance.option('value');
  //     analysis_name_instance.option('value', '');

  //     //Get the instance of the TreeView 
  //     let selection_treeview = document.getElementById("selection-treeview");
  //     let selection_treeview_instance = TreeView.getInstance(selection_treeview) as TreeView;

  //     //Generate ID according to the current No Of Nodes
  //     let currentChild = this.Devices[this.currentIndex].items[3].items.length;
  //     currentChild == 0 ? currentChild += 1 : '';
  //     let genId = this.currentId  + "_" + currentChild;

  //     if(value == ''){
  //       this.Devices[this.currentIndex].items[3].items.push({
  //         id: genId + '_' + 1,
  //         text: name,
  //         items:[]        
  //       });
  //     }

  //     // let obj =  this.Devices[this.currentIndex].items[3].items;
  //     // if(obj.length > 0){
  //     //   for(let i=0; i<obj.length; i++){
  //     //     if(genId == obj[i].id){
  //     //       this.Devices[this.currentIndex].items[3].items[0].items.push({
  //     //         id: genId + '_' + 1,
  //     //         text: name,
  //     //         items:[]        
  //     //       })
  //     //     }
  //     //   }
  //     // }
  //     // else{
  //     //   console.log("First Time")
  //     //   //Push Item in the List
  //     //   this.Devices[this.currentIndex].items[3].items.push({
  //     //     id: genId,
  //     //     text: value,
  //     //     items:[{
  //     //       id: genId + '_' + 1,
  //     //       text: name,
  //     //       items:[]        
  //     //     }]
  //     //   })
  //     // }

  //     //Update List
  //     selection_treeview_instance.option('items', this.Devices);

  //     this.currentId = '';
  //   }
  //   else{
  //     e.preventDefault;
  //   }

  // }




  // syncTreeViewSelection() {
  //   if (!this.treeView) return;

  //   if (!this.treeBoxValue) {
  //       this.treeView.instance.unselectAll();
  //   } else {
  //       this.treeView.instance.selectItem(this.treeBoxValue);
  //   }
  // }

  // getSelectedItemsKeys(items) {
  //   var result = [],
  //       that = this;

  //   items.forEach(function(item) {
  //       if(item.selected) {
  //           result.push(item.key);
  //       }
  //       if(item.items.length) {
  //           result = result.concat(that.getSelectedItemsKeys(item.items));
  //       }
  //   });
  //   return result;
  // }

  // treeView_itemSelectionChanged(e){
  //   const nodes = e.component.getNodes();
  //   this.treeBoxValue = this.getSelectedItemsKeys(nodes).join("");
  // }

}
