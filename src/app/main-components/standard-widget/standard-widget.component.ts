import { Component, OnInit, Input, ViewChild, Output, EventEmitter  } from '@angular/core';
import { OptomizationComponent } from 'src/app/routes/optomization/optomization.component';

@Component({
  selector: 'app-standard-widget',
  templateUrl: './standard-widget.component.html',
  styleUrls: ['./standard-widget.component.scss']
})
export class StandardWidgetComponent implements OnInit {
  @Input() widgetTitle: string = 'Widget Title';
  @Input() widgetHeader: boolean = true;
  @Input() collapse: boolean = true;
  @Input() collapsePanel: boolean = true;
  @Input() NoPadding: boolean = false;
  @Input() BI_Container: boolean = false;
  @Input() show_addWidgetPanel: boolean = false;
  @Input() show_settingsPanel:boolean = false;
  @Input() overflowHide:boolean = false;
  @Output() addWidget = new EventEmitter();
  @Output() deleteWidget=new EventEmitter();
  @Input() widgetItem:any;
  constructor() {  }

  ngOnInit(): void {
  }

  addNewWidget(){
this.addWidget.emit();
  }

  removeWidget(){
    this.deleteWidget.emit(this.widgetItem);
    console.log("Removing the widget");
  }


}
