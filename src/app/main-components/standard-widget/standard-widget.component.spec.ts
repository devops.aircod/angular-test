import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardWidgetComponent } from './standard-widget.component';

describe('StandardWidgetComponent', () => {
  let component: StandardWidgetComponent;
  let fixture: ComponentFixture<StandardWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
