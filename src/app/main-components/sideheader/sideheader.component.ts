import { Component, OnInit } from '@angular/core';
import { $ } from 'protractor';

@Component({
  selector: 'airview-sideheader',
  templateUrl: './sideheader.component.html',
  styleUrls: ['./sideheader.component.scss']
})

export class SideheaderComponent implements OnInit {
  public menuState:boolean = false;
  // public showDropdown:boolean = false;
  public menuItems = [
    {
      "Id":"1",
      "Name":"Survey",
      "Icon":"fa fa-check",
      "Child": [
        {
          "Id":"1_1",
          "Name":"Survey List",
          "Icon":"fa fa-check"
        },
        {
          "Id":"1_2",
          "Name":"Survey Document",
          "Icon":"fa fa-check"
        },
        {
          "Id":"1_3",
          "Name":"Survey Clone",
          "Icon":"fa fa-check"
        }
      ]
    },
    {
      "Id":"2",
      "Name":"Project Management",
      "Icon":"fa fa-chart-line"
    },
    {
      "Id":"3",
      "Name":"Ticket Management",
      "Icon":"fa fa-ticket-alt"
    }
  ]


  constructor() { }

  ngOnInit(): void {
  }

  showDropdown(e, data){
    console.log(e, data)
    let child = data.Child;
    if(child.length > 0){
      return true;
    }
    else{
      return false;
    }
  }


}
