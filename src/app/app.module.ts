import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // Included For Bootstrap Dropdown Module

import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { DxTreeViewModule } from 'devextreme-angular';
import { DxPopoverModule, DxTemplateModule, DxPopupModule } from 'devextreme-angular';
import { DxSelectBoxModule, DxTextBoxModule, DxButtonModule, DxValidatorModule, DxValidationGroupModule, DxDropDownBoxModule, DxContextMenuModule } from 'devextreme-angular';



import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { HeaderComponent } from './main-components/header/header.component';
import { SideheaderComponent } from './main-components/sideheader/sideheader.component';
import { OptomizationComponent } from './routes/optomization/optomization.component';
import { StandardWidgetComponent } from './main-components/standard-widget/standard-widget.component';
import { AnalysisPanelComponent } from './main-components/analysis-panel/analysis-panel.component';
import { GridsterModule, GridsterComponent } from 'angular-gridster2';



const appRoutes: Routes = [
  { path: '', component: OptomizationComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SideheaderComponent,
    OptomizationComponent,
    StandardWidgetComponent,
    AnalysisPanelComponent
  ],
  imports: [
    BrowserModule,
    TooltipModule.forRoot(),
    TabsModule.forRoot() ,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    AppRoutingModule,
    DxTreeViewModule,
    DxPopoverModule,
    DxTemplateModule,
    DxSelectBoxModule,
    DxTextBoxModule,
    DxButtonModule,
    DxValidatorModule,
    DxValidationGroupModule,
    DxDropDownBoxModule,
    DxContextMenuModule,
    DxPopupModule,
    GridsterModule
  ],
  providers: [GridsterComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
