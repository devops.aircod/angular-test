import { Component, ViewChild, OnInit, Input } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { GridsterConfig, Draggable, Resizable, PushDirections, GridsterItem, GridType, CompactType, DisplayGrid } from 'angular-gridster2';

interface Safe extends GridsterConfig {
  draggable: Draggable;
  resizable: Resizable;
  pushDirections: PushDirections;
}

@Component({
  selector: 'airview-optomization',
  templateUrl: './optomization.component.html',
  styleUrls: ['./optomization.component.scss']
})
export class OptomizationComponent implements OnInit {

  options: Safe;
  dashboard: Array<GridsterItem>;
  @ViewChild('tabset') tabset: TabsetComponent;
  selectedItem: any;

  widgetMenuOpen: boolean = false;
  isAddTab: boolean = false; //To Show or Hide Add New Tab TextBox
  tabName: string; // Name of the Newly added Tabs
  currentSelected: any;
  tabList: any = []; // List of Dynamically Added Tabs
  groups: any[] = [
    { "name": "Analysis View", "id": "1" },
    { "name": "Other View", "id": "2" }
  ]
  items: any = [];
  showDeleteConfirmation: boolean = false;

  constructor() {
    this.items = [{
      text: 'Rename',
    },
    { text: 'Delete' },

    ];
  }


  // Bootstrap Tabs
  goto(id) {
    this.tabset.tabs[id].active = true;
  }

  listClick(event, newValue) {
    this.selectedItem = newValue;  // don't forget to update the model here
    // ... do other stuff here ...
  }

  ngOnInit(): void {
    this.options = {
      gridType: GridType.ScrollVertical,
      compactType: CompactType.None,
      margin: 10,
      outerMargin: true,
      outerMarginTop: null,
      outerMarginRight: null,
      outerMarginBottom: null,
      outerMarginLeft: null,
      useTransformPositioning: true,
      mobileBreakpoint: 640,
      minCols: 1,
      maxCols: 12,
      minRows: 1,
      maxRows: 100,
      maxItemCols: 100,
      minItemCols: 1,
      maxItemRows: 100,
      minItemRows: 1,
      maxItemArea: 2500,
      minItemArea: 1,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedColWidth: 105,
      fixedRowHeight: 105,
      keepFixedHeightInMobile: false,
      keepFixedWidthInMobile: false,
      scrollSensitivity: 10,
      scrollSpeed: 20,
      enableEmptyCellClick: false,
      enableEmptyCellContextMenu: false,
      enableEmptyCellDrop: false,
      enableEmptyCellDrag: false,
      enableOccupiedCellDrop: false,
      emptyCellDragMaxCols: 50,
      emptyCellDragMaxRows: 50,
      ignoreMarginInRow: false,
      draggable: {
        enabled: true,
      },
      resizable: {
        enabled: true,
      },
      swap: false,
      pushItems: true,
      disablePushOnDrag: false,
      disablePushOnResize: false,
      pushDirections: { north: true, east: true, south: true, west: true },
      pushResizeItems: false,
      displayGrid: DisplayGrid.None,
      disableWindowResize: false,
      disableWarnings: false,
      scrollToNewItems: false
    };

    this.dashboard = [
      { cols: 2, rows: 1, y: 0, x: 0, label: 'Chart Widget' },
      { cols: 2, rows: 2, y: 0, x: 4, label: 'Table Widget' },
      { cols: 2, rows: 2, y: 0, x: 5, minItemRows: 2, minItemCols: 2, label: 'Map Widget' },
      { cols: 2, rows: 2, y: 1, x: 0, maxItemRows: 2, maxItemCols: 2, label: 'Tab Widget' },
      { cols: 2, rows: 1, y: 2, x: 2, dragEnabled: true, resizeEnabled: true, label: 'Page Widget' },
      { cols: 2, rows: 1, y: 2, x: 4, dragEnabled: false, resizeEnabled: false, label: 'Another Widget' },
    ];

  }

  changedOptions() {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
  }

  removeItem(item) {
   
    // $event.preventDefault();
    // $event.stopPropagation();
    this.dashboard.splice(this.dashboard.indexOf(item), 1);
  }

  addItem() {
    this.dashboard.push({ x: 0, y: 0, cols: 1, rows: 1, icon:"fa fa-cross" });
  }



  /**
  * // TODO: For SHOWING AND HIDING ADD NEW TAB BUTTON
  */
  addNewTab() {
    if (!this.isAddTab) {
      this.isAddTab = true;
    } else {
      this.isAddTab = false;
    }
    this.tabName="";
    this.selectedItem="";
  }

  /**
  * // TODO: Saving New Tab
  */
  saveNewTab(event) {
    let result = event.validationGroup.validate();
    if (result.isValid) {
      if (this.selectedItem) {
         this.isAddTab = false;
        const newState = this.groups.map(obj =>
          obj.id === this.selectedItem.id ? { id: this.selectedItem.id, name: this.tabName } : obj
        );
        this.groups = newState;
        this.selectedItem = false;
      } else {
        let lastIndex = this.groups.pop();
        this.groups.push(lastIndex);
        this.tabList.push({ heading: this.tabName, id: +lastIndex.id + 1 });
        this.groups.push({ name: this.tabName, id: +lastIndex.id + 1 });
        this.isAddTab = false;
        this.tabName = "";
        this.selectedItem = null;
      }
    }
  }

  itemClick(event) {
    if (event.itemIndex === 0) {
      if (!this.isAddTab) {
        this.isAddTab = true;
        this.tabName = this.selectedItem.name;
      }
    } else if (event.itemIndex === 1) {
      this.showDeleteConfirmation = true;
    }
  }

  updateContextItems(event) {
    this.selectedItem = event;
  }

   /**
  * // TODO: Deleting the Tab
  */
  deleteTab(event) {
    if (this.showDeleteConfirmation) {
      let index = this.groups.findIndex(e => +e.id === +this.selectedItem.id);
      this.groups.splice(index, 1);
      let index1 = this.tabList.findIndex(e => +e.id === +this.selectedItem.id);
      if (index1) {
        this.tabList.splice(index1, 1);
      }
      this.showDeleteConfirmation = false;
      this.tabName="";

    }


  }
  cancel(event) {
    if (this.showDeleteConfirmation) {
      this.showDeleteConfirmation = false;
    }

  }
}
