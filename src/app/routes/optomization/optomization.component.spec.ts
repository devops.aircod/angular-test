import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptomizationComponent } from './optomization.component';

describe('OptomizationComponent', () => {
  let component: OptomizationComponent;
  let fixture: ComponentFixture<OptomizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptomizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptomizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
